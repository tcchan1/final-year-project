﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public GameObject [] panel1;
	public GameObject [] panel2;
	public GameObject help;
	public GameObject [] sound;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//remove panel 1
	public void RemovePanelOne()
	{
		for(int i = 0; i < panel1.Length; i++)
		{
			panel1[i].SetActive(false);		// deactive button on panel 1
		}
	}

	//remove panel 2
	public void RemovePanelTwo()
	{
		for(int i = 0; i < panel2.Length; i++)
		{
			panel2[i].SetActive(false);		// deactive button on panel 1
		}
	}

	//add panel 1
	public void AddPanelOne()
	{
		for(int i = 0; i < panel1.Length; i++)
		{
			panel1[i].SetActive(true);		// deactive button on panel 1
		}
	}

	//add panel 2
	public void AddPanelTwo()
	{
		for(int i = 0; i < panel2.Length; i++)
		{
			panel2[i].SetActive(true);		// deactive button on panel 1
		}
	}


	// Click Map Select
	public void btnMapSelect()
	{
		sound[0].audio.Play();
		RemovePanelOne();
		AddPanelTwo();
	}

	// Click infinity mode
	public void btnInfinityMode()
	{
		sound[0].audio.Play();
		Application.LoadLevel(1);
	}
	public void btnPanelTwoBack()
	{
		sound[0].audio.Play();
		RemovePanelTwo();
		AddPanelOne();
	}
	public void btnMap1()
	{
		sound[0].audio.Play();
		Application.LoadLevel(2);
	}
	public void btnMap2()
	{
		sound[0].audio.Play();
		Application.LoadLevel(3);
	}
	public void btnMap3()
	{
		sound[0].audio.Play();
		Application.LoadLevel(4);
	}
	public void btnMap4()
	{
		sound[0].audio.Play();
		Application.LoadLevel(5);
	}
	public void btnIntro()
	{
		sound[0].audio.Play();
		help.SetActive(true);
	}

	public void picHelp()
	{
		sound[0].audio.Play();
		help.SetActive(false);
	}
}
