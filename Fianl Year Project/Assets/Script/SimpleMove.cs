﻿using UnityEngine;
using System.Collections;

public class SimpleMove : MonoBehaviour {

	public float speed;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKey(KeyCode.W))
		{
			Vector3 temp = new Vector3(0.0f, 0.0f, speed);
			gameObject.transform.position += (temp * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.S)) 
		{		
			Vector3 temp = new Vector3(0.0f, 0.0f, -speed);
			gameObject.transform.position += (temp * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.A))
		{
			Vector3 temp = new Vector3(-speed, 0.0f, 0.0f);
			gameObject.transform.position += (temp * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D))
		{
			Vector3 temp = new Vector3(speed, 0.0f, 0.0f);
			gameObject.transform.position += (temp * Time.deltaTime);
		}
	}
}
