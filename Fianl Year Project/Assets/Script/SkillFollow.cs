﻿using UnityEngine;
using System.Collections;

public class SkillFollow : MonoBehaviour {
	public GameObject player;
	// Use this for initialization
	void Start () {
		player = GameObject.Find("Charactor1");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp = player.transform.position;
		Quaternion temp2 = player.transform.rotation;
		gameObject.transform.position = temp;
		gameObject.transform.rotation = temp2;
	}
}
