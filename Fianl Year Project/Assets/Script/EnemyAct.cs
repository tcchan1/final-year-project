﻿using UnityEngine;
using System.Collections;

public class EnemyAct : MonoBehaviour {

	public GameObject target;
	public GameObject cam;
	public GameObject dieEffect;
	public GameObject spawnEffect;	
	public GameObject[] attack;
	GameObject gm;
	Animator anim;

	public bool enemyFound, canAttack;

	public float currentTime, lastTime, lastAttackTime, attackDelay;
	float lastAngle, currentAngle;

	// Use this for initialization
	void Start () {
		Instantiate(spawnEffect, gameObject.transform.position, gameObject.transform.rotation);
		target = GameObject.Find("Charactor1");
		anim = GetComponent<Animator>();
		gm = GameObject.Find("GameManager");
		cam = GameObject.Find("Main Camera");
		enemyFound = false;

		searchEnmey();
		currentTime = Time.time;
		lastTime = Time.time;

		currentAngle = gameObject.transform.rotation.y;
		StartMoveAnim();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		currentTime = Time.time;
		isNearEnemy();
		if(enemyFound)
		{
			LookAtTarget();
			MoveToTarget();
			TryToAttack();
		}
		else
		{
			searchEnmey();
			if(currentTime > lastTime+1)
			{
				RandomAngle();
			}
		}

	}

	public void isNearEnemy()
	{
		if(gameObject.transform.position.x > target.transform.position.x - 3 && gameObject.transform.position.x < target.transform.position.x + 3)
		{
			if(gameObject.transform.position.z > target.transform.position.z - 3 && gameObject.transform.position.z < target.transform.position.z + 3)
			{
				cam.GetComponent<CamMove>().isEnemyNear = true;
			}
		}
	}

	void LookAtTarget()
	{
		float tempX, tempZ, tempAngle;
		tempX = target.transform.position.x - gameObject.transform.position.x;
		tempZ = target.transform.position.z - gameObject.transform.position.z;

		tempAngle = Mathf.Atan2(tempX, tempZ) * Mathf.Rad2Deg;

		if(currentAngle <  tempAngle + 2)
		{
			currentAngle += 3f;
			gameObject.transform.rotation = Quaternion.AngleAxis(currentAngle , Vector3.up);
		}
		if(currentAngle >  tempAngle - 2)
		{
			currentAngle -= 3f;
			gameObject.transform.rotation = Quaternion.AngleAxis(currentAngle , Vector3.up);
		}
	}

	void MoveToTarget()
	{
		//Move Forward
		gameObject.transform.Translate(Vector3.forward * Time.deltaTime);
		StopAttackAnim();
	}

	// Attack strategy
	void TryToAttack()
	{
		if(gameObject.transform.position.x > target.transform.position.x - 1 && gameObject.transform.position.x < target.transform.position.x + 1)
		{
			if(gameObject.transform.position.z > target.transform.position.z - 1 && gameObject.transform.position.z < target.transform.position.z + 1)
			{
				if(Time.time > lastAttackTime+3f)
				{
					//Instantiate(attack[0], gameObject.transform.position, gameObject.transform.localRotation);
					canAttack = true;
					lastAttackTime = Time.time;
					attackDelay = Time.time;
					StartAttackAnim();
				}
				if(Time.time > attackDelay + 1.0f && canAttack)
				{
					canAttack = false;
					Instantiate(attack[0], gameObject.transform.position, gameObject.transform.localRotation);
				}
			}
		}
	}

	void searchEnmey()
	{

		// Random direction to go
		if(currentAngle < lastAngle)
		{
			currentAngle += 1;
			gameObject.transform.rotation = Quaternion.AngleAxis(currentAngle , Vector3.up);
		}

		//Move Forward
		gameObject.transform.Translate(Vector3.forward * Time.deltaTime);



		//Find Target
		if(currentAngle > target.transform.rotation.y + 15 || currentAngle < target.transform.rotation.y - 15)
		{
			enemyFound = true;
		}
	}

	void RandomAngle()
	{
		lastAngle = currentAngle + Random.Range(-80, 80);
	}
	// make animation
	void StartAttackAnim()
	{
		anim.SetBool("CanAttack",true);
	}
	void StopAttackAnim()
	{
		anim.SetBool("CanAttack",false);
	}
	void StartMoveAnim()
	{
		anim.SetBool("Move",true);
	}
	void StopMoveAnim()
	{
		anim.SetBool("Move",false);
	}

	//hit with something
	void OnCollisionEnter(UnityEngine.Collision hit)
	{
		if(hit.gameObject.tag == "DeadZone")
		{
			Instantiate(dieEffect, gameObject.transform.position, gameObject.transform.rotation);
			Debug.Log("Die");
			gm.GetComponent<GameManager>().EnemyDie();
			cam.GetComponent<CamMove>().isEnemyNear = false;
			Destroy(gameObject);
		}

	}
	void OnTriggerEnter(Collider hit)
	{
		if(hit.gameObject.tag == "Skill")
		{
			Instantiate(dieEffect, gameObject.transform.position, gameObject.transform.rotation);
			gm.GetComponent<GameManager>().EnemyDie();
			cam.GetComponent<CamMove>().isEnemyNear = false;
			Destroy(gameObject);
		}
		if(hit.gameObject.tag == "TrapDamage")
		{
			Instantiate(dieEffect, gameObject.transform.position, gameObject.transform.rotation);
			gm.GetComponent<GameManager>().EnemyDie();
			cam.GetComponent<CamMove>().isEnemyNear = false;
			Destroy(gameObject);
		}

	}

}
