﻿using UnityEngine;
using System.Collections;

public class Floor_Trap : MonoBehaviour {

	public GameObject[] spears;
	public float height;
	public bool isIn;
	public Collision target;
	// Use this for initialization
	void Start () {
		isIn = false;
	}
	
	// Update is called once per frame
	void Update () {

	}
	void OnCollisionEnter (Collision col)
	{
		isIn = true;
		target = col;
		ActiveTrap ();
		/*
		GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
		cam.transform.parent = null;
		Destroy (col.gameObject);
		*/
	}

	void OnCollisionExit (Collision col)
	{
		isIn = false;
		target = null;
		DisactiveTrap ();
	}

	public void ActiveTrap()
	{
		for (int i = 0; i < spears.Length; i++) 
		{
			Vector3 temp = new Vector3(0.0f, height, 0.0f);
			spears[i].transform.position += (temp * Time.deltaTime);
		}
	}

	public void DisactiveTrap()
	{
		for (int i = 0; i < spears.Length; i++) 
		{
			Vector3 temp = new Vector3(0.0f, -height, 0.0f);
			spears[i].transform.position += (temp * Time.deltaTime);
		}
	}
}
