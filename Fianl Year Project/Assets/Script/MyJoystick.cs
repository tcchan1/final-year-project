﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MyJoystick : MonoBehaviour {

	float width;
	float height;
	string text;
	public bool isMoving;
	public int fingerID;
	public float speed;
	public GameObject player;
	public GameObject[] Skills;
	Vector3 startPos;
	Vector3 currentPos;
	float angle;
	float deltaX, deltaY;
	int state;				// 0 = idel  , 1 = walk

	// Use this for initialization
	void Start () {
		initialize();
		getScreenSize();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		touchToMove();
		isFingerIn();
		MovingWithKeyboard();
		//showFingerID();
	}

	void initialize()
	{
		isMoving = false;
		fingerID = -1;
		angle = 0;
		state = 0;
		player.animation.CrossFade("Wait");
	}
	bool IsInLeft(Vector2 pos)
	{
		if(pos.x < (width/2))
		{
			return true;
		}
		return false;
	}
	private void getScreenSize()
	{
		width = Screen.width;
		height = Screen.height;
		//GameObject.Find("TitleText").GetComponent<Text>().text = "Screen Size = "+width+"x"+height;
	}

	void touchToMove()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			if(IsInLeft(Input.GetTouch(i).position))
			{
				switch(fingerID)
				{
				case -1:
					fingerID = Input.GetTouch(i).fingerId;
					startPos = Input.GetTouch(i).position;
					break;
				case 0:
					currentPos = Input.GetTouch(i).position;
					break;
				case 1:
					currentPos = Input.GetTouch(i).position;
					break;
				default:
					fingerID = Input.GetTouch(i).fingerId;
					startPos = Input.GetTouch(i).position;
					break;
				}
				isMoving =true;
				Moving();
			}
		}
	}
	void isFingerIn()
	{
		if(Input.touchCount == 0)
		{
			isMoving = false;
			fingerID = -1;
			if(state != 0){
				state = 0;
				player.animation.CrossFade("Wait");
			}
		}
		else
		{
			bool tmp = true;
			for(int i = 0; i < Input.touchCount; i++)
			{
				if(IsInLeft(Input.GetTouch(i).position))
				{
					tmp = false;
				}
			}
			if(tmp)
			{
				isMoving = false;
				fingerID = -1;
				if(state != 0){
					state = 0;
					player.animation.CrossFade("Wait");
				}
			}
		}
	}
	void Moving()
	{
		deltaX = currentPos.x - startPos.x;
		deltaY = currentPos.y - startPos.y;
		Vector3 tmpPos = player.transform.position;
		angle = Mathf.Atan2(deltaX, deltaY) * Mathf.Rad2Deg;
		player.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
		player.transform.Translate(Vector3.forward * Time.deltaTime*1.5f);
		if(state != 1){
			state = 1;
			player.animation.CrossFade("Walk");
		}
	}
	void MovingWithKeyboard()
	{
		if(Input.GetKey(KeyCode.W)&&Input.GetKey(KeyCode.D))
		{
			angle = 45;
			walk();
		}else if(Input.GetKey(KeyCode.S)&&Input.GetKey(KeyCode.D))
		{
			angle = 135;
			walk();
		}else if(Input.GetKey(KeyCode.S)&&Input.GetKey(KeyCode.A))
		{
			angle = 225;
			walk();
		}else if(Input.GetKey(KeyCode.W)&&Input.GetKey(KeyCode.A))
		{
			angle = 315;
			walk();
		}else if(Input.GetKey(KeyCode.W))
		{
			angle = 0;
			walk();
		}else if(Input.GetKey(KeyCode.A))
		{
			angle = 270;
			walk();
		}else if(Input.GetKey(KeyCode.S))
		{
			angle = 180;
			walk();
		}else if(Input.GetKey(KeyCode.D))
		{
			angle = 90;
			walk();
		}
	}
	void walk()
	{
		player.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
		player.transform.Translate(Vector3.forward * Time.deltaTime*1.5f);
		if(state != 1){
			state = 1;
			player.animation.CrossFade("Walk");
		}
	}
	void showFingerID()
	{
		string tmp = "";
		tmp += "IsMoving = "+isMoving;
		tmp += "\nFingerID = "+fingerID;
		tmp += "\nStartPos = "+startPos;
		tmp += "\nCurrentPos = "+currentPos;
		tmp += "\n deltaX = "+deltaX;
		tmp += "\n deltaY = "+deltaY;
		tmp += "\nAngle = " + angle;
		//GameObject.Find("TextOnLeft").GetComponent<Text>().text = tmp;
	}
}
