﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public int score;
	public int waveNum;
	public int numPlayer;		//player number
	public int totalEnemy;		//toatl enemy number
	public int MaxEnemyInGame;	//maximum enemy in game
	public int currentEnemyNum; //number of enemy in battle area
	public int killedEnemy;		//number of killed enemy
	public int initEnemyNum;
	public int sceneNum;

	public bool isEnd;
	public bool isShow;
	public bool infinityMode;

	float endTime;
	float lastEnemySpwanTime;

	public GameObject[] ui;		// 0 = waveUI, 1 = scoreUI, 2 = enemy left
	public GameObject btnMenu;
	public GameObject btnRetry;
	public GameObject [] sound;

	public GameObject winImage;
	public GameObject loseImage;

	public GameObject[] enemy;

	// Use this for initialization
	void Start () {
		isEnd = false;
		isShow = false;
		endTime = 0;
		lastEnemySpwanTime = Time.time;
		ui[2].GetComponent<Text>().text = "Wave "+waveNum+"\n"+totalEnemy+" Enemies";
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		UpdateUI();		// update ui text



		if(!isEnd)
		{
			CheckEnd();
			if(currentEnemyNum == 0 && killedEnemy == 0)		// it is start, spawn enemy
			{
				if(Time.time > lastEnemySpwanTime + 3)
				{
					SpawnEnemy(initEnemyNum,0);
					lastEnemySpwanTime = Time.time;
					ui[2].SetActive(false);
				}
			}
			else
			{
				spawnEnemyByTime(0);
			}
		}
		if(!isShow && isEnd)
		{
			if(infinityMode && numPlayer > 0)
			{
				InfinityMode();
			}
			if(endTime + 1 < Time.time )
			{
				GameOver();
				if(numPlayer <1 )
				{
					Lose();
				}else
				{
					if(!infinityMode)
					{
						Win();
					}
				}
			}
		}
	}

	// prepare for start
	void ReadyToStart()
	{

	}

	// check is end
	void CheckEnd()
	{
		if(totalEnemy <= killedEnemy || numPlayer < 1)
		{
			isEnd = true;
			endTime = Time.time;
		}
	}

	// Game End
	void GameOver()
	{
		GetComponent<MyJoystick>().enabled = false;
		// Remove skill button and add reload button
		GameObject.Find("SkillOne").SetActive(false);
		GameObject.Find("SkillTwo").SetActive(false);
		GameObject.Find("SkillThree").SetActive(false);
		GameObject.Find("DraggingArea").SetActive(false);

	}

	// Do when win
	void Win()
	{
		winImage.SetActive(true);
		btnMenu.SetActive(true);
		btnRetry.SetActive(true);
	}
	// Do when Lose
	void Lose()
	{
		loseImage.SetActive(true);
		btnMenu.SetActive(true);
		btnRetry.SetActive(true);
	}

	public void Reload()
	{
		sound[0].audio.Play();
		Application.LoadLevel(sceneNum);
	}

	public void Menu()
	{
		sound[0].audio.Play();
		Application.LoadLevel(0);
	}

	void SpawnEnemy(int enemyToSpawn, int enemyType)
	{
		int tempCol = GetComponent<MapGenerater>().numOfColumn;
		int tempRow = GetComponent<MapGenerater>().numOfRow;
		for(int i = 0; i < enemyToSpawn; i++)
		{
			Vector3 tempPos = new Vector3(Random.Range(5.0f, tempCol - 5.0f), 0.1f, Random.Range(5.0f, tempRow - 5.0f));		// random position to respawn
			Quaternion tempRotation = new Quaternion(0.0f, Random.Range(0.0f, 359.9f), 0.0f, 1.0f);		// random rotation to respawn
			Instantiate(enemy[enemyType], tempPos, gameObject.transform.rotation);
			currentEnemyNum += 1;
		}
	}

	public void EnemyDie()
	{
		killedEnemy += 1;
		currentEnemyNum -=1;
		score += 20+waveNum*5;
		if(currentEnemyNum < MaxEnemyInGame)
		{
			SpawnEnemy(1,0);
		}
	}

	void spawnEnemyByTime(int enemyType)
	{
		if(Time.time > lastEnemySpwanTime + 3)
		{
			if(currentEnemyNum < MaxEnemyInGame)
			{
				SpawnEnemy(1,enemyType);
				lastEnemySpwanTime = Time.time;
			}
		}
	}

	void UpdateUI()
	{
		ui[0].GetComponent<Text>().text = "Wave " + waveNum;
		ui[1].GetComponent<Text>().text = "Score : " + score;

	}

	void InfinityMode()
	{
		isEnd = false;
		waveNum +=1;
		currentEnemyNum = 0;
		killedEnemy = 0;
		totalEnemy = (int)Mathf.FloorToInt(totalEnemy *1.3f);
		MaxEnemyInGame += 2;
		initEnemyNum = 3 + (int)Mathf.FloorToInt(waveNum /3);
		ui[2].SetActive(true);
		ui[2].GetComponent<Text>().text = "Wave "+waveNum+"\n"+totalEnemy+" Enemies";
		lastEnemySpwanTime = Time.time;

		ClearEnemy();
		if(waveNum %5 == 0)
		{
			gameObject.GetComponent<MapGenerater>().numOfColumn += 5;
			gameObject.GetComponent<MapGenerater>().numOfRow += 5;
			gameObject.GetComponent<MapGenerater>().Rebuild();
		}
	}
	void ClearEnemy()
	{
		//Clear Enemy
		currentEnemyNum = 0;
		GameObject [] tempEnemy;
		tempEnemy = GameObject.FindGameObjectsWithTag("Enemy");
		for(int i = 0; i < tempEnemy.Length; i++)
		{
			Destroy(tempEnemy[i]);
		}
	}
}
