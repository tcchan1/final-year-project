﻿using UnityEngine;
using System.Collections;

public class CamMove : MonoBehaviour {

	public GameObject player;
	Vector3 tempPos;
	public Vector3 camDistance;

	public bool isEnemyNear;

	// Use this for initialization
	void Start () {
		camDistance = new Vector3(0.0f, 2.5f, 5.0f);
		tempPos = player.transform.position;
		gameObject.transform.position = new Vector3(tempPos.x+camDistance.x, tempPos.y+camDistance.y, tempPos.z-camDistance.z);
	}
	
	// Update is called once per frame
	void Update () {
		tempPos = player.transform.position;
		gameObject.transform.position = new Vector3(tempPos.x+camDistance.x, tempPos.y+camDistance.y, tempPos.z-camDistance.z);

		NearEnemyZoom();
	}

	void WallZoom()
	{
		if(player.transform.position.z < 8)
		{
			if(camDistance.z >2.0f)
			{
				camDistance.z -= 0.15f;
			}
			if(camDistance.y >1.5f)
			{
				camDistance.y -= 0.05f;
			}
		}else
		{
			if(camDistance.z <5.0f)
			{
				camDistance.z += 0.15f;
			}
			if(camDistance.y <2.5f)
			{
				camDistance.y += 0.05f;
			}
		}
	}

	void NearEnemyZoom()
	{
		if(isEnemyNear)
		{
			if(camDistance.z >3f)
			{
				camDistance.z -= 0.06f;
			}
			if(camDistance.y >2f)
			{
				camDistance.y -= 0.02f;
			}
		}
		else
		{
			WallZoom();
		}
	}
}
