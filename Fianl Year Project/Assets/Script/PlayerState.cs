﻿using UnityEngine;
using System.Collections;

public class PlayerState : MonoBehaviour {

	GameObject gm;
	public GameObject dieEffect;
	public GameObject spawnEffect;

	// Use this for initialization
	void Start () {
		gm = GameObject.Find("GameManager");
		RandomAppear();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(UnityEngine.Collision hit)
	{
		if(hit.gameObject.tag == "DeadZone")
		{
			Debug.Log("Die");
			gm.GetComponent<GameManager>().numPlayer -= 1;
			Destroy(gameObject);
		}
	}

	void RandomAppear()
	{
		int tempCol = gm.GetComponent<MapGenerater>().numOfColumn;
		int tempRow = gm.GetComponent<MapGenerater>().numOfRow;

		Vector3 tempPos = new Vector3(Random.Range(5.0f, tempCol - 5.0f), 0.1f, Random.Range(5.0f, tempRow - 5.0f));		// random position to respawn
		Quaternion tempRotation = new Quaternion(0.0f, Random.Range(0.0f, 359.9f), 0.0f, 1.0f);		// random rotation to respawn
		gameObject.transform.position = tempPos;
		gameObject.transform.rotation = tempRotation;
		Instantiate(spawnEffect, gameObject.transform.position, gameObject.transform.rotation);
	}
	void OnTriggerEnter(Collider hit)
	{
		// trigger traps
		if(hit.gameObject.tag == "TrapDamage")
		{
			Instantiate(dieEffect, gameObject.transform.position, gameObject.transform.rotation);
			gm.GetComponent<GameManager>().numPlayer -= 1;
			Destroy(gameObject);
		}

		//attack by enemy
		if(hit.gameObject.tag == "EnemyAttack")
		{
			Instantiate(dieEffect, gameObject.transform.position, gameObject.transform.rotation);
			gm.GetComponent<GameManager>().numPlayer -= 1;
			Destroy(gameObject);
		}
		
	}
}
