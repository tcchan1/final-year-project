﻿using UnityEngine;
using System.Collections;

public class EnemySkillFollow : MonoBehaviour {

	public GameObject target;
	// Use this for initialization
	void Start () {
		target = GameObject.Find("Enemy1");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp =target.transform.position;
		Quaternion temp2 = target.transform.rotation;
		gameObject.transform.position = temp;
		gameObject.transform.rotation = temp2;
	}
}
