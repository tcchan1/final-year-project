﻿using UnityEngine;
using System.Collections;

public class MapGenerater : MonoBehaviour {

	public float spacing;
	public int numOfRow;
	public int numOfColumn;
	public GameObject[] floors;
	public int[] floorMulti;

	private int[,] ground;


	// Use this for initialization
	void Start () 
	{
		GenerateMap();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}
	
	public void GenerateMap()
	{
		ground = new int[numOfRow, numOfColumn];
		for (int i = 0; i < numOfRow; i++) 
		{
			for (int j = 0; j < numOfColumn; j++)
			{
				if(i <3 || i > numOfRow-3 || j <3 || j > numOfColumn-3)
				{
					ground[i,j] = Random.Range(0, 3);
				}
				else
				{
					int max =0;
					for(int k = 3; k < floorMulti.Length; k++)
					{
						max += floorMulti[k];
					}
					int temp = Mathf.FloorToInt(Random.Range(1.0f, max+1));
					max = 0;
					int last = 0;
					for(int k = 3; k < floorMulti.Length; k++)
					{
						max += floorMulti[k];
						if(temp >last && temp <= max)
						{
							ground[i,j] = k;
						}
						last = max;
					}
					//ground[i,j] = Mathf.FloorToInt(Random.Range(0.0f,floors.Length *1.0f));
				}
				Vector3 pos = new Vector3(i, 0, j) * spacing;
				Instantiate(floors[ground[i,j]], pos, Quaternion.identity);
			}
		}
	}
	public void ClearMap()
	{
		GameObject[] temp;
		temp  = GameObject.FindGameObjectsWithTag("Floor");
		for (int i = 0; i< temp.Length; i++) 
		{
			Destroy (temp[i]);
		}
	}
	public void Rebuild()
	{
		ClearMap();
		GenerateMap();
	}
}
