﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CombineButton : MonoBehaviour {

	string text;
	public GameObject controller;
	public GameObject draggingArea;
	public GameObject[] buttons;
	public GameObject player;
	Vector3 draggingAreaPos;
	Vector3 oriPos;
	bool isdraged;
	int skillToCast; // 0 = no skill to cast, 1 = button 1, 2 = button 2, 3 = button 3, 4 = button 1+2, 5 = button 1+3, 6 = button 2+3, 7 = button 1+2+3

	// Use this for initialization
	void Start () {
		text = "No Skill Cast";
		oriPos = gameObject.transform.position;
		isdraged = false;
		skillToCast = 0;
		draggingAreaPos = draggingArea.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Update on fixed frame rate
	void FixedUpdate() 
	{
		if(isdraged)
		{
			Moving();
		}
	}
	void showMsg()
	{
		//GameObject.Find("TextOnRight").GetComponent<Text>().text = text;
	}
	public void beingDrag()
	{
		text = "being Dragged";
		switch(gameObject.name)
		{
		case "SkillOne":
			skillToCast = 1;
			Debug.Log("1");
			break;
		case "SkillTwo":
			skillToCast = 2;
			Debug.Log("2");
			break;
		case "SkillThree":
			skillToCast = 3;
			Debug.Log("3");
			break;
		default:
			skillToCast = 0;
			break;
		}
		isdraged = true;
		showMsg();
	}
	public void endDrag()
	{
		if(isInArea())
		{
			Vector3 tempPos = controller.GetComponent<MyJoystick>().player.transform.position;
			switch(skillToCast)
			{
			case 1:
				text = "Cast SkillOne";
				tempPos = new Vector3(tempPos.x, tempPos.y + 0.5f, tempPos.z);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[0], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				break;
			case 2:
				text = "Cast SkillTwo";
				
				tempPos = new Vector3(tempPos.x, tempPos.y + 0.5f, tempPos.z);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[1], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				break;
			case 3:
				text = "Cast SkillThree";
				tempPos = new Vector3(tempPos.x, tempPos.y+ 0.3f , tempPos.z);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[2], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				break;
			case 4:
				text = "Cast SkillOne + SkillTwo";
				tempPos = new Vector3(tempPos.x, tempPos.y + 0.5f, tempPos.z + 2.0f);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[3], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				//Instantiate(controller.GetComponent<MyJoystick>().Skills[1], controller.GetComponent<MyJoystick>().player.transform.position, controller.GetComponent<MyJoystick>().player.transform.rotation);
				break;
			case 5:
				tempPos = new Vector3(tempPos.x, tempPos.y + 2.0f, tempPos.z);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[4], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				//Instantiate(controller.GetComponent<MyJoystick>().Skills[2], controller.GetComponent<MyJoystick>().player.transform.position, controller.GetComponent<MyJoystick>().player.transform.rotation);
				text = "Cast SkillOne + Skill Three";
				break;
			case 6:
				tempPos = new Vector3(tempPos.x, tempPos.y + 0.5f, tempPos.z + 2.0f);
				Instantiate(controller.GetComponent<MyJoystick>().Skills[5], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);
				//Instantiate(controller.GetComponent<MyJoystick>().Skills[2], controller.GetComponent<MyJoystick>().player.transform.position, controller.GetComponent<MyJoystick>().player.transform.rotation);
				text = "Cast SkillTwo + SkillThree";
				break;
			case 7:
				Instantiate(controller.GetComponent<MyJoystick>().Skills[6], tempPos, controller.GetComponent<MyJoystick>().player.transform.rotation);

				text = "Cast SkillOne + SkillTwo + SkillThree";
				break;
			default:
				text = "Cast Default Skill";
				break;

			}
			//playing attack animation
			player.animation["Attack"].wrapMode = WrapMode.Once;
			int tempstate = 0;
			if(player.animation.IsPlaying("Walk"))
			{
				tempstate = 1;
			}
			player.animation.CrossFade("Attack");
			if(tempstate == 0)
			{
				player.animation.CrossFadeQueued("Wait");
			}
			else
			{
				player.animation.CrossFadeQueued("Walk");
			}

		}
		showMsg();
		isdraged = false;

		for(int i = 0; i < buttons.GetLength(0); i++)
		{
			buttons[i].transform.position = buttons[i].GetComponent<CombineButton>().oriPos;
		}
	}
	void Moving()
	{
		switch(controller.GetComponent<MyJoystick>().fingerID)
		{
		case -1:
			try{
			gameObject.transform.position = Input.GetTouch(0).position;
			}catch{
				gameObject.transform.position = Input.mousePosition;
				combineSkill();
			}
			break;
		case 0:
			gameObject.transform.position = Input.GetTouch(1).position;
			break;
		case 1:
			gameObject.transform.position = Input.GetTouch(0).position;
			break;
		default:
			gameObject.transform.position = Input.GetTouch(0).position;
			break;
		}
		combineSkill();
	}
	// check if the button dropped into draggingArea
	bool isInArea()
	{
		// check  button is near dragging area position x and y
		if(gameObject.transform.position.x > (draggingAreaPos.x - 65) && gameObject.transform.position.x < (draggingAreaPos.x + 65))
		{
			if(gameObject.transform.position.y > (draggingAreaPos.y - 65) && gameObject.transform.position.y < (draggingAreaPos.y + 65))
			{
				return  true;
			}
		}
		return false;
	}
	void combineSkill()
	{
		for(int i = 0; i < buttons.GetLength(0); i++)
		{
			if(gameObject.transform.position.x > (buttons[i].transform.position.x - 30) && gameObject.transform.position.x < (buttons[i].transform.position.x + 30))
			{
				if(gameObject.transform.position.y > (buttons[i].transform.position.y - 30) && gameObject.transform.position.y < (buttons[i].transform.position.y + 30))
				{
					if(i == 0 )
					{
						switch(skillToCast)
						{
						case 1:
							break;
						case 2:
							skillToCast = 4;
							break;
						case 3:
							skillToCast = 5;
							break;
						case 4:
							break;
						case 5:
							break;
						case 6 :
							skillToCast = 7;
							break;
						case 7:
							break;
						default:
							break;

						}
					}else if(i == 1)
					{
						switch(skillToCast)
						{
						case 1:
							skillToCast = 4;
							break;
						case 2:
							break;
						case 3:
							skillToCast = 6;
							break;
						case 4:
							break;
						case 5:
							skillToCast = 7;
							break;
						case 6 :
							break;
						case 7:
							break;
						default:
							break;
							
						}
					}else if(i == 2)
					{
						switch(skillToCast)
						{
						case 1:
							skillToCast = 5;
							break;
						case 2:
							skillToCast = 6;
							break;
						case 3:
							break;
						case 4:
							skillToCast = 7;
							break;
						case 5:
							break;
						case 6 :
							break;
						case 7:
							break;
						default:
							break;
							
						}
					}
				}
			}
		}
	}
}