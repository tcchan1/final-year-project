﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TouchManager : MonoBehaviour {

	float width;
	float height;
	string text;
	public int moveFID;
	public bool isDragging;

	// Use this for initialization
	void Start () 
	{
		getScreenSize();
		initialize();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	
	//Update on fixed frame rate
	void FixedUpdate() 
	{
		moveOnLeft();
	}

	private void getScreenSize()
	{
		width = Screen.width;
		height = Screen.height;
		GameObject.Find("TitleText").GetComponent<Text>().text = "Screen Size = "+width+"x"+height;
	}

	private void moveOnLeft()
	{
		string tmp = "No Touch detected on Left";
		isDragging = false;
		if(!IsMoveFingerIn())
		{
			for (int i = 0; i < Input.touchCount; ++i) 
			{
				Vector2 tmpPos = Input.GetTouch(i).position;
				if(IsInLeft(tmpPos))
				{
					isDragging = true;
					tmp = "Touch Detected";
					moveFID = Input.GetTouch(i).fingerId;
					tmp = tmp + "\nFingerID = "+moveFID;
					tmp = tmp + "\nPosition = x = "+tmpPos.x+" y = "+tmpPos.y;
					tmp = tmp + "\nDebug : x = "+tmpPos.x+", width/2 = "+(width/2);
				}
			}
		}else
		{
			for (int i = 0; i < Input.touchCount; ++i) 
			{
				if(Input.GetTouch(i).fingerId == moveFID)
				{
					Vector2 tmpPos = Input.GetTouch(i).position;
					if(IsInLeft(tmpPos))
					{
						isDragging = true;
						tmp = "Touch Detected";
						moveFID = Input.GetTouch(i).fingerId;
						tmp = tmp + "\nFingerID = "+moveFID;
						tmp = tmp + "\nPosition = x = "+tmpPos.x+" y = "+tmpPos.y;
						tmp = tmp + "\nDebug : x = "+tmpPos.x+", width/2 = "+(width/2);
					}
				}
			}
		}
		if(Input.touchCount == 0)
		{
			moveFID = -1;
		}
		GameObject.Find("TextOnLeft").GetComponent<Text>().text = tmp;
	}

	private bool IsMoveFingerIn()
	{
		if(moveFID == -1)
		{
			return false;
		}else
		{
			for (int i = 0; i < Input.touchCount; ++i) 
			{
				if(Input.GetTouch(i).fingerId == moveFID)
				{
					return true;
				}
			}
			return false;
		}
	}
	private bool IsInLeft(Vector2 pos)
	{
		if(pos.x < (width/2))
		{
			return true;
		}
		return false;
	}

	private void initialize()
	{
		moveFID = -1;
	}


}
